@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="{{route('blog.create')}}" class="btn btn-sm btn-warning" >Добавить</a>


                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-bordered">
                    <tr>
                        <td>
                            Название
                        </td>
                        <td>
                            Описание
                        </td>
                        <td>
                            Автор
                        </td>
                        <td>
                            Действии
                        </td>
                    </tr>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>
                                {{$blog->topic}}
                            </td>
                            <td>
                                {{$blog->description}}
                            </td>
                            <td>
                                {{$blog->getAuthor()->name}}
                            </td>
                            <td>
                                <a class="btn btn-sm btn-primary" href="{{ route('blog.show',$blog->id) }}">Просмотреть</a>
                                @if($blog->author_id == auth()->user()->id)
                                    <form action="{{ route('blog.destroy',$blog->id) }}" method="POST">
                                        <a class="btn btn-sm btn-warning" href="{{ route('blog.edit',$blog->id) }}">Изменить</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Удалить</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
