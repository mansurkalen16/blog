@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Изменить блог</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('blog.index') }}">Назад</a>
                        </div>
                    </div>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('blog.update', $blog->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Topic:</strong>
                                <input type="text" name="topic" class="form-control" value="{{$blog->topic}}" placeholder="Topic">
                                <input type="hidden" name="author_id" class="form-control" value="{{auth()->user()->id}}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Description:</strong>
                                <textarea class="form-control"  name="description" placeholder="Description">{{$blog->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Text:</strong>
                                <textarea class="form-control"  name="text" placeholder="Text">{{$blog->text}}</textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Изменить</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
