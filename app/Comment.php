<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id', 'blog_id', 'answer_for', 'text',
    ];
    public function getAuthor(){
        return $this->hasOne(User::class, 'id', 'author_id')->first();
    }
    public function getBlog(){
        return $this->hasOne(Blog::class, 'id', 'blog_id')->first();
    }
    public function getAnswerFor(){
        return $this->hasOne(Comment::class, 'id', 'answer_for')->first();
    }
}
