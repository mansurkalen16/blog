<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id', 'topic', 'description', 'text',
    ];

    public function getAuthor(){
        return $this->hasOne(User::class, 'id', 'author_id')->first();
    }
    public function getComments(){
        return $this->hasMany(Comment::class, 'blog_id', 'id')->get();
    }
}
